import smtplib
from storage import Storage
from email.mime.text import MIMEText
from passwords import db_user_pwd

class MailTo(object):
    """Класс для отправки почты"""
    def login(self):
        """Логинимся в почту"""
        smtpObj = smtplib.SMTP('mail.ant-konsalt.ru', 25)
        smtpObj.login('spb\deploy_database', db_user_pwd)
        return smtpObj

    def send(self):
        """Формируем и отправляем письмо"""
        smtpObj = self.login()
        text = Storage().print_storage()
        text = MIMEText(text.encode('utf-8'), 'plain', 'utf-8')
        text = text.as_string()
        msg = "\r\n".join((
            "From: %s" % 'deploy_database@ant-konsalt.ru',
            "To: %s" % 'v.kotov@ant-konsalt.ru',
            "Subject: %s" % 'Deploy Database 1C',
            text
        ))

        smtpObj.sendmail('deploy_database@ant-konsalt.ru','v.kotov@ant-konsalt.ru',msg)

MailTo().send()
