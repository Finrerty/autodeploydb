"""
Основной модуль, принимает данные с .html страницы, распоряжается ими
Здесь, очевидно, должно быть много проверок входных данных, я их добавлю
"""


import cgi
import codecs
import sys

from collections import OrderedDict


class Storage(object):
    _fields = OrderedDict()

    def set_fields(self):
        sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
        form = cgi.FieldStorage()
        self._fields['project_name'] = form.getvalue('project_name', 'NotSet')
        self._fields['region'] = form.getvalue('region', 'NotSet')
        self._fields['objective'] = form.getvalue('objective', 'NotSet')
        self._fields['applicant_name'] = form.getfirst('applicant_name', 'NotSet')
        self._fields['request'] = form.getfirst('request', '83')
        self._fields['extract_folder'] = format('/mnt/share_ftp{}'.format(
            form.getfirst('extract_folder', 'NotSet')
        ))
        self._fields['base_file'] = form.getfirst('base_file', 'NotSet')
        self._fields['msg'] = form.getfirst('msg', 'NotSet')

    def get_field(self, field_name):
        result_field = self._fields[field_name]
        return result_field

    def print_storage(self):
        sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
        result = ''
        for i, v in self._fields.items():
            result += '{}: {}\n'.format(i, v)

        return result
