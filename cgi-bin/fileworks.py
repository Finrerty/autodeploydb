"""
Модуль для работы с файлами. Определяет какой тип файла поступил в обработку,
в зависимости от типа решает что с ним делать.
К .zip и .rar еще добавится 7z.

Логика сводится к распаковке архива в зависимости от его расширения, переносу
файла в папку, которая находится на необходимом сервере и непосредственное
обращение к модулю разворота базы.

"""


from abc import ABCMeta, abstractmethod
import os
import zipfile
import shutil

import rarfile

from storage import Storage
from services import deploy


class FileType(metaclass=ABCMeta):
    _types = {}
    _count = 0

    def __init__(self, source, filename, destination):
        self.source = source
        self.filename = filename
        self.destination = destination

    @classmethod
    def add_type(cls, name):
        def decorator(klass):
            cls._types[name] = klass

            return klass

        return decorator

    def get_instance(self, filename, *args, **kwargs):
        name, ext = os.path.splitext(str(filename).lower())
        ext = ext.lstrip('.')
        klass = self._types.get(ext)
        return klass(self.source, filename, self.destination, *args, **kwargs)

    def execute(self):
        pass


@FileType.add_type('zip')
class RarFileType(FileType):
    def execute(self):
        zf = zipfile.ZipFile('{}{}'.format(self.source, self.filename))

        zf.extractall(self.destination)

        for f in zf.infolist():
            if f.filename:
                file_name = f.filename
                break
            else:
                raise EnvironmentError

        self.source = self.destination
        klass = FileType(self.source, file_name, self.destination)
        work_file = klass.get_instance(file_name)
        work_file.execute()


@FileType.add_type('rar')
class RarFileType(FileType):
    def execute(self):
        rf = rarfile.RarFile('{}{}'.format(self.source, self.filename))

        rf.extractall(self.destination)

        for f in rf.infolist():
            if f.filename:
                file_name = f.filename
                break
            else:
                raise EnvironmentError

        klass = FileType(self.destination, file_name, self.destination)
        work_file = klass.get_instance(file_name)
        work_file.execute()

@FileType.add_type('dt')
class DtFileType(FileType):
    def execute(self):
        pass


@FileType.add_type('bak')
class BakFileType(FileType):
    def execute(self):
        if self.source != self.destination:
            shutil.copy('{}{}'.format(self.source, self.filename), self.destination)

        baseinfo = Storage()
        project = baseinfo.get_field('project_name')
        region = baseinfo.get_field('region')
        request = baseinfo.get_field('request')
        deploy(self.filename, project, region, request)
