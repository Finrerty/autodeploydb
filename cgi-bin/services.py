"""
Модуль для работы с СУБД и (возможно) еще парой сервисов.
В дальнейшем будет реализован класс, так как базы по разным проектам
разворачиваются на разных серверах, соответственно для каждого будет свой
объект соединения.
"""


import pymssql

from passwords import supp_sql_pwd


def deploy(filename, project, region, request):
    """Функция, отвечающая за работу с SQL(разворот БД)"""
    SQL_DEPLOY_DB = """
DECLARE @fileListTable TABLE
(
    LogicalName          NVARCHAR(128),
    PhysicalName         NVARCHAR(260),
    [Type]               CHAR(1),
    FileGroupName        NVARCHAR(128),
    Size                 NUMERIC(20,0),
    MaxSize              NUMERIC(20,0),
    FileID               BIGINT,
    CreateLSN            NUMERIC(25,0),
    DropLSN              NUMERIC(25,0),
    UniqueID             UNIQUEIDENTIFIER,
    ReadOnlyLSN          NUMERIC(25,0),
    ReadWriteLSN         NUMERIC(25,0),
    BackupSizeInBytes    BIGINT,
    SourceBlockSize      INT,
    FileGroupID          INT,
    LogGroupGUID         UNIQUEIDENTIFIER,
    DifferentialBaseLSN  NUMERIC(25,0),
    DifferentialBaseGUID UNIQUEIDENTIFIER,
    IsReadOnly           BIT,
    IsPresent            BIT,
    TDEThumbprint        VARBINARY(32),
    SnapshotURL          NVARCHAR(128)
)

--This schema works from SQL 2008 to SQL 2014.
--SQL 2005 didn't have the TDEThumbprint column, but is otherwise the same.

INSERT INTO @fileListTable EXEC('restore filelistonly
FROM DISK = N''F:\BackUp\\autodeploy\\{}''')

DECLARE @datafile NVARCHAR(128), @logfile NVARCHAR(128)

SELECT @datafile = LogicalName FROM @fileListTable WHERE Type = 'D'

SELECT @logfile = LogicalName FROM @fileListTable WHERE Type = 'L'

RESTORE DATABASE [{}_{}_{}]
FROM DISK = N'F:\BackUp\\autodeploy\\{}'
WITH RECOVERY,
MOVE @datafile TO N'E:\MSSQL12.MSSQLSERVER\MSSQL\DATA\\{}_{}_{}.mdf',
MOVE @logfile TO N'E:\MSSQL12.MSSQLSERVER\MSSQL\DATA\\{}_{}_{}_Log.ldf'
""".format(filename, project, region, request, filename, project,
           region, request, project, region, request)

    conn = pymssql.connect(
        host='192.168.198.30',
        user='sa',
        password=supp_sql_pwd,
        autocommit=True
    )

    with conn:
        cursor = conn.cursor()
        cursor.execute(SQL_DEPLOY_DB)

