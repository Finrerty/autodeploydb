"""
Данный модуль - фабрика классов. Для каждого проекта будет
сделана своя реализация, имеющая свои отличительные особенности.
"""


from fileworks import FileType
from storage import Storage


class ProjectFactory(object):
    _projects = {}

    @classmethod
    def add_project(cls, name):
        def decorator(klass):
            cls._projects[name] = klass

            return klass
        return decorator

    @classmethod
    def execute(cls, name, *args, **kwargs):
        klass = cls._projects.get(name)
        return klass(*args, **kwargs).execute()


@ProjectFactory.add_project('АИСРГ')
class AisrgProject(object):
    def __init__(self):
        pass
    def execute(self):
        print('AIS RG')


@ProjectFactory.add_project('РНГ')
class AisrngProject(object):
    def __init__(self):
        pass
    def execute(self):
        params_getter = Storage()
        source = params_getter.get_field('extract_folder')
        filename = params_getter.get_field('base_file')
        destination = '/mnt/ant-supp-sql/share_sql/'
        work_file = FileType(source, filename, destination)
        result_file = work_file.get_instance(filename)
        result_file.execute()


@ProjectFactory.add_project('Аналитика')
class AnalitikaProject(object):
    def __init__(self):
        pass
    def execute(self):
        print('Analitika')

