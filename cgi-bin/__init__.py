#!/usr/bin/env python3
from storage import Storage
from mail import MailTo
from projects import ProjectFactory


Storage().set_fields()
MailTo().send()
project = Storage().get_field('project_name')
ProjectFactory().execute(project)
